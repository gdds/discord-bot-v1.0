const fs = require('fs')
const CDP = require('chrome-remote-interface');
const zlib = require('zlib-sync');
const State = require("./state.js");

class Browser {
    constructor(cli, port = 9222){
        this.cli = cli
        this.port = port
    }

    async connect() {
        await this.connectCDP()
        this.state = new State(this.cli)
        this.listenXHR()
        this.listenWebsocket()
        this.loadDiscord()
    }

    async connectCDP() {
        this.client = await CDP({ host: 'localhost', port: this.port })
        await this.client.Network.enable();
    }

    async loadDiscord() {
        await this.client.Page.enable();
        await this.client.Page.navigate({ url: 'https://discord.com/channels/@me' });
        await this.client.Page.loadEventFired();
    }

    async listenXHR() {
        this.client.Network.responseReceived((response) => {
            if (response.type == 'XHR') {
                if (response.response.url.match(/\/users\/(\d+)\/profile/)) {
                    // console.info(response.response.url, response.requestId)
                    // Example:
                    // this.client.Network.getResponseBody({ requestId: response.requestId }).then((data) => {
                    //     doSomehting(data.body)
                    // })
                }
            }
        })
    }

    setupWSInflate() {
        this.inflate = new zlib.Inflate({
            chunkSize: 65535,
            flush: zlib.Z_SYNC_FLUSH,
        });
    }

    async listenWebsocket() {
        this.setupWSInflate()

        this.client.Network.webSocketFrameReceived((response) => {
            if (response.response.opcode != 1) {
                let data = Buffer.from(response.response.payloadData, 'base64')
                const l = data.length;
                const flush = l >= 4 &&
                    data[l - 4] === 0x00 &&
                    data[l - 3] === 0x00 &&
                    data[l - 2] === 0xFF &&
                    data[l - 1] === 0xFF;
                this.inflate.push(data, flush && zlib.Z_SYNC_FLUSH)
                if (this.inflate.result) {
                    this.handleWSPacket(this.inflate.result)
                } else {
                    this.setupWSInflate()
                }
            }
        })
    }

    handleWSPacket(result) {
        // console.info('WS', result.toString())
        let data
        try {
            data = JSON.parse(result.toString())
        } catch { return }
        if (data.t == 'GUILD_MEMBER_LIST_UPDATE') {
            this.state.updateGuildMembers(data.d)
        } else if (data.t == 'READY') {
            this.state.setReadyPacket(data);
        } else if (data.t == 'MESSAGE_CREATE') {
            this.state.addMessage(data.d);
        } else if (data.t == 'CHANNEL_CREATE') {
            this.state.channelCreate(data.d);
        } else if (data.t == 'PRESENCE_UPDATE') {
            this.state.presenceUpdate(data.d);
        }
        if (data.t) {
            if (global.debug) console.info(data.t, data.d)
            if (global.debug) fs.writeFileSync(`tmp/packets/${data.t}_${+ new Date()}.json`, JSON.stringify(data.d))
        }

    }

    async getChats() {
        return await this.execFileScript('getChats.js')
    }

    async goToChat(chatId){
        return await this.execFileScript('goToChat.js', chatId)
    }

    async awaitMembers() {
        return await this.execFileScript('awaitMembers.js')
    }

    async readMember(){
        let result = await this.execFileScript('readMember.js')
        return { result }
    }

    async startSearch() {
        return await this.execFileScript('startSearch.js')
    }

    async confirmSearch(username) {
        return await this.execFileScript('confirmSearch.js', username)
    }

    async scrollMembers(){
        return await this.execFileScript('scrollMembers.js')
    }

    async execFileScript(filename, args = ''){
        let script = fs.readFileSync(`./snippets/${filename}`)
        let preparedScript = script.toString().replace("'----ARGUMENTS----'", JSON.stringify(args));
        // console.info(preparedScript)
        let result = await this.client.send('Runtime.evaluate', { expression: preparedScript, awaitPromise: true });
        if (result.result.subtype == 'error') console.info('execFileScript result for file ', filename, 'with args', args, 'is', result)
        let resultData
        try {
            resultData = JSON.parse(result.result.value)
        } catch(e) {
            console.info("Cant parse result", result.result.value)
        }
        return resultData
    }
}

module.exports = Browser
