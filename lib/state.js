const fs = require('fs')

class State {
  constructor(cli) {
    this.cli = cli
    this.readyPacket = {}
    this.members = []
    this.messages = []
    this.userData = []
    this.private_channels
    this.freshUserData = []
    setInterval(() => this.postFreshUserData(), 30000)
    // setInterval(() => this.postOnlines(), 60000)
  }

  setReadyPacket(data) {
    this.readyPacket = data
    this.private_channels = data.d.private_channels
    this.postReadyPacket()
  }

  syncGuildMembers(op, guildId) {
    let items = op.items
    if (!items) items = [op.item]
    items.forEach(memberItem => {
      if (memberItem.member) {
        if (!memberItem.member.guilds_list) memberItem.member.guilds_list = []
        if (!memberItem.member.guilds_list.includes(guildId)) memberItem.member.guilds_list.push(guildId)

        this.pushOrReplaceUser(memberItem)
      }
    });
  }

  pushOrReplaceUser(memberItem) {
    let index = this.userData.findIndex(m => m.member.user.id == memberItem.member.user.id)
    if (index == -1) {
      this.userData.push(memberItem)
    } else {
      this.userData[index] = memberItem
    }
    this.freshUserData.push(memberItem)
  }

  updateGuildMembers(d) {
    let guildId = d.guild_id
    d.ops.forEach(op => {
      if (op.op == 'SYNC' || op.op == 'UPDATE' || op.op == 'INSERT') {
        this.syncGuildMembers(op, guildId)
      }
    });
  }

  addMessage(data) {
    if (data.guild_id) return
    if (!data.channel_id) return
    if (data.author.id == this.readyPacket.d.user.id) {
      data.user_id = this.private_channels.find(c => c.id == data.channel_id)?.recipient_ids[0]
    }
    this.messages.push(data)
    this.postMessage(data)
  }

  channelCreate(data) {
    if (data.guild_id) return
    if (this.private_channels.find(c => c.id == data.id)) return
    data.recipient_ids = data.recipients.map(r => r.id)
    if (global.debug) console.info('adding channel', data)
    this.private_channels.push(data)
  }

  presenceUpdate(data) {
    let member = this.userData.find(u => u.member.user.id == data.user.id)
    if (member) member.member.presence = data
    this.freshUserData.push(member) // update presence of user in freshUsers
  }

  ///

  postReadyPacket() {
    if (global.debug) fs.writeFileSync('tmp/ready.json', JSON.stringify(this.readyPacket))
    this.cli.postReadyPacket(this.readyPacket)
  }

  postMessage(data) {
    this.cli.postMessage(data)
  }

  postFreshUserData() {
    let payload = []
    let item
    while (item = this.freshUserData.shift()){
      payload.push(item)
    }
    if (payload.length == 0) return

    this.cli.postMembers(payload)
  }

  postOnlines() {
    let payload = this.userData.filter(u => u.member.presence.status != 'offline').map(u => u.member.user.id)
    if (payload.length == 0) return

    this.cli.postOnlines(payload)
  }
}

module.exports = State
