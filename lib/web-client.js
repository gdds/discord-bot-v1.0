const fetch = require('node-fetch');

class WebClient {
  constructor() {
    this.baseUrl = 'http://localhost:3000/api/v1'
    this.token = 'test'
  }

  async postReadyPacket(packet) {
    this.id = packet.d.user.id
    return await this.post('/bots/ready_packet', packet)
  }

  async postMembers(packet) {
    return await this.post(`/bots/${this.id}/members`, packet)
  }

  async postOnlines(packet) {
    return await this.post(`/bots/${this.id}/onlines`, packet)
  }

  async jobs(id) {
    return await this.get(`/bots/${id}/jobs`)
  }

  async postJobResult(id, result) {
    return await this.post(`/jobs/${id}/result`, result)
  }

  async postJobResultFast(id, boolResult) {
    let result = {
      status: boolResult ? 'success' : 'error'
    }
    await this.postJobResult(id, result)
  }

  async postMessage(message) {
    return await this.post(`/bots/${this.id}/message`, message)
  }

  async post(enpdpoint, packet) {
    const response = await fetch(
      `${this.baseUrl}${enpdpoint}`,
      {
        method: 'post',
        body: JSON.stringify({ packet: packet }),
        headers: {
          'Content-type': 'application/json',
          'Authorization': `Token ${this.token}`
        }
      }
    )

    return await response.json();
  }

  async get(enpdpoint) {
    const response = await fetch(
      `${this.baseUrl}${enpdpoint}`,
      {
        method: 'get',
        headers: {
          'Authorization': `Token ${this.token}`
        }
      }
    )

    return await response.json();
  }
}

module.exports = WebClient
