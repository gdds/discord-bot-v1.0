var Browser = require("./lib/browser.js")
var robot = require("robotjs");
var repl = require("repl");
const WebClient = require("./lib/web-client.js");

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

class DiscordWindow {
    constructor(){
        this.cli = new WebClient()
    }

    async setupBrowser(){
        this.browser = new Browser(this.cli)
        await this.browser.connect()
    }

    async init() {
        this.chats = await this.browser.getChats()
        var attempts = 0
        while (this.chats.length < 1 && attempts < 20) {
            console.info('Can not find discord window. Waiting for it opens..', this.chats)
            this.chats = await this.browser.getChats()
            attempts++
            await sleep(1000)
        }
        return this.chats.length > 0
    }

    async readMembers() {
        let attempts = 0
        while (attempts < 5) {
            var memberData = await this.browser.readMember()
            console.info(memberData.result, this.browser.state.userData.length)
            if (memberData.result == 0) {
                attempts += 1;
                await this.browser.scrollMembers()
            } else {
                attempts = 0;
            }
        }
    }

    async startChat(memberType, memberSelect) {
        await this.browser.goToChat('/channels/@me')
        await this.browser.startSearch()
        await robot.typeString(memberType)
        return await this.browser.confirmSearch(memberSelect)
    }

    async sendMessage(memberType, memberSelect, message) {
        let result = await this.startChat(memberType, memberSelect)
        console.info('startChatResult', result)
        if (!result) return false;
        await sleep(2000)
        let lines = message.split(/[\r\n]+/)
        for (let i = 0; i < lines.length; i++) {
            await robot.typeString(lines[i])
            await robot.keyTap("enter", "shift")
            await sleep(100)
        }
        await sleep(500)
        await robot.keyTap("enter")
        await sleep(2000)
        return true
    }

    async loadServer(server) {
        await this.setupBrowser()
        await this.init()
        await sleep(5000)
        await this.browser.goToChat(`/channels/${server}/1`)
        console.info('goToChat Done')
        await sleep(3000)
        let chatResult = await this.browser.awaitMembers()
        console.info('chatResult =', chatResult)
        if (!chatResult) {
            console.info('goToChat Fail')
            return false;
        }
        console.info('Members loaded')
        await this.readMembers()
        await this.cli.postMembers(this.browser.state.userData)
        return true;
    }

    async runJobs() {
        let jobs = await this.cli.jobs(this.browser.state.readyPacket.d.user.id)
        console.info('jobsToPerform', jobs)
        for (let i = 0; i < jobs.length; i++) {
            let job = jobs[i]
            if (job.method == 'message') {
                let result = await this.sendMessage(job.params.to_search, job.params.to_result, job.params.message)
                await this.cli.postJobResultFast(job.id, result)
            } else if (job.method == 'loadServer') {
                let result = await this.loadServer(job.params.server);
                await this.cli.postJobResultFast(job.id, result)
            }
        }
        setTimeout(() => this.runJobs(), 10000)
    }
}


async function main() {
    let discordWindow = new DiscordWindow()
    global.d = discordWindow
    // await discordWindow.loadServer('348449397308391426')
    // await discordWindow.loadServer('785578837442297946')
    // await discordWindow.loadServer('552464554563731456')
    await discordWindow.loadServer('404994858160226304')
    discordWindow.runJobs()
    repl.start("node> ").context.d = discordWindow
}

global.debug = true
main()


// all roles:
// d.browser.readyPacket.d.guilds.map(g => g.roles).reduce((a, b) => a.concat(b), []).map(a => [a.id, a.name])

// d.browser.userDataShort.map(member => member.roles).filter((x) => x)

// var names = d.browser.userDataShort.filter((x) => x.member.guilds_list[0] == '351703230499258372').map((x) => `${x.member.user.username}#${x.member.user.discriminator}`)
// [...new Set(names)]

// var names = d.browser.userDataShort.filter((x) => x.member.guilds_list[0] == '351703230499258372').map((x) => x.member.nick || x.member.user.username)

// var names = d.browser.userDataShort.filter((x) => x.member.guilds_list[0] == '351703230499258372').map((x) => `${x.member.nick || x.member.user.username} [${x.member.user.username}#${x.member.user.discriminator}]`)
