(async function () {
  let waitForAndSleep = function (condition, sleep = 0, freq = 500) {
    return new Promise(resolve => {
      let doCheck = () => {
        if (condition()) setTimeout(resolve, sleep)
        else setTimeout(doCheck, freq)
      }
      doCheck()
    })
  }

  let searchBox = document.querySelector("[class^='searchBarComponent-']")
  searchBox.click()
  await waitForAndSleep(() => document.querySelector("[class^='quickswitcher-']"))
  return 0
})()
