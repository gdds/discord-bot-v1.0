(async function() {
    let waitForAndSleep = function (condition, sleep = 0, freq = 500) {
        return new Promise(resolve => {
            let doCheck = () => {
                if (condition()) setTimeout(resolve, sleep)
                else setTimeout(doCheck, freq)
            }
            doCheck()
        })
    }
    let members = document.querySelector("[class^='members-']")
    members.scrollTop += 1000
    await waitForAndSleep(() => true, 1000 + Math.random(500))
    return 0
})()
