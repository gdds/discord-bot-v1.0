(function() {
    let waitForAndSleep = function (condition, sleep = 0, freq = 500) {
        return new Promise(resolve => {
            let doCheck = () => {
                if (condition()) setTimeout(resolve, sleep)
                else setTimeout(doCheck, freq)
            }
            doCheck()
        })
    }

    let pillHeight = function(element) {
        var parent = element
        var i = 0;
        while (i < 10 && !parent.className.toString().startsWith('listItem-')){
            parent = parent.parentNode;
            i++;
        }
        if (parent.className.toString().startsWith('listItem-')) {
            let pill = parent.querySelectorAll('[class^="pill-"]')[0]
            if (pill) {
                return pill.offsetHeight
            } else {
                return 0
            }
        }
        return 0;
    }
    let args = '----ARGUMENTS----'
    let url = args
    let selector
    let urlMatch = url.match(/^(.+channels\/\d+\/)\d+/)
    if (urlMatch) {
        selector = `[href^="${urlMatch[1]}"]`
    } else {
        selector = `[href^="${url}"]`
    }
    let chat = document.querySelectorAll(selector)[0]
    if (!chat) {
        console.info('NO chat for', selector)
        return Promise.resolve(0)
    }
    chat.click()
    return new Promise(resolve => {
        waitForAndSleep(() => pillHeight(chat) > 10).then(() => resolve(0))
    })
})()
