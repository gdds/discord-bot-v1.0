(async function () {
    let waitForAndSleep = function (condition, sleep = 0, freq = 500, maxAttempts = 1000) {
        let attempts = 0
        return new Promise(resolve => {
            let doCheck = () => {
                if (attempts++ > maxAttempts || condition()) setTimeout(resolve, sleep)
                else setTimeout(doCheck, freq)
            }
            doCheck()
        })
    }
    let sleep = function(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    if (!window.currentMemberIndex) window.currentMemberIndex = 0
    let minMember = document.querySelector("[class^='members-'] [class^='member-']")
    if (!minMember) {
        return 0;
    }
    let minIndex = +minMember.getAttribute('index')
    if (window.currentMemberIndex < minIndex) {
        window.currentMemberIndex = minIndex
    }
    let members = document.querySelector("[class^='members-']")
    var member = members.querySelector("[class^='member-'][index='" + window.currentMemberIndex + "']")
    if (!member) {
        return 0
    }
    let nameElement = member.querySelector("[class^='name-']")
    if (!nameElement) {
        return -1
    }
    let name = nameElement.textContent
    window.currentMemberIndex++
    return JSON.stringify(name)
})()
