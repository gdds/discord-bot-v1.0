(async function() {
    let waitForAndSleep = function (condition, sleep = 0, freq = 500, maxAttempts = 1000) {
        let attempts = 0
        return new Promise((resolve, reject) => {
            let doCheck = () => {
                if (attempts++ > maxAttempts) {
                    reject('Attempts reached')
                } else {
                    let result = condition()
                    if (result) setTimeout(() => resolve(result), sleep)
                    else setTimeout(doCheck, freq)
                }
            }
            doCheck()
        })
    }

    try {
        await waitForAndSleep(() => {
            let members = document.querySelectorAll("[class^='members-']")
            if (members.length > 0) {
                return members[0].querySelectorAll("[class^='member-']").length > 0
            }
            return false
        }, 0, 500, 20)
        return 1;
    } catch(e) {
        return 0;
    }
})()
