(async function () {
  let waitForAndSleep = function (condition, sleep = 0, freq = 500, maxAttempts = 1000) {
    let attempts = 0
    return new Promise((resolve, reject) => {
      let doCheck = () => {
        if (attempts++ > maxAttempts) {
          reject('Attempts reached')
        } else {
          let result = condition()
          if (result) setTimeout(() => resolve(result), sleep)
          else setTimeout(doCheck, freq)
        }
      }
      doCheck()
    })
  }

  let searchResult = document.querySelector("[role^='listbox']")
  let searchText = '----ARGUMENTS----'
  let element
  try {
    element = await waitForAndSleep(() => {
      let items = [...searchResult.querySelectorAll("[class^='username-']")]
      let item = items.find((x) => x.innerText.toString().indexOf(searchText) != -1)
      console.info('items', items.map((x) => x.innerText.toString()))
      return item
    }, 0, 500, 20)
  } catch(e) {
    console.info('off attempts', element)
    return 0
  }
  console.info('want to click', element)
  element.click()
  return 1
})()
