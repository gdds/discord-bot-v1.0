(function() {
    let chats = Array.from(document.querySelectorAll('[role="tree"] [class^=wrapper][href]'))
    let result = chats.map((x) => { 
        let object = { name: x.getAttribute('aria-label'), href: x.getAttribute('href') }
        let chatRoot = x.parentNode.parentNode.parentNode
        let badget = chatRoot.querySelector('[class^=numberBadge]')
        if (badget) {
            object.unread = +badget.innerText
        }
        return object
    })
    return JSON.stringify(result)
})()